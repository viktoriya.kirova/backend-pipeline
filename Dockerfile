FROM php:8.0-apache

WORKDIR /var/www/html

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC

ARG CI_JOB_ID
ARG CI_COMMIT_REF_NAME
ENV CI_JOB_ID ${CI_JOB_ID}
ENV CI_COMMIT_REF_NAME ${CI_COMMIT_REF_NAME}

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN sed -i -e "s+/var/www/html+$APACHE_DOCUMENT_ROOT+g" /etc/apache2/sites-available/*.conf \
    && sed -i -e "s+/var/www/+${APACHE_DOCUMENT_ROOT}/+g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN apt update && apt -y upgrade \
    && DEBIAN_FRONTEND=noninteractive apt install -y \
       gnupg2 \
       curl \
       git \
       zip \
       unzip \
       libfreetype6-dev \
       libjpeg62-turbo-dev \
       libpng-dev \
       libgd-dev \
       librecode-dev \
       libcurl4-openssl-dev \
       libzip-dev \
       pkg-config \
       libssl-dev \
       libxml2-dev \
       libonig-dev \
       wget \
    && apt -y autoremove && apt autoclean && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install zip

RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install pdo_mysql xml mbstring curl gd \
    && echo "upload_max_filesize = 100M;\npost_max_size = 100M\n" >> $PHP_INI_DIR/conf.d/php-laravel.ini

RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - \
    && apt-get install -y nodejs

RUN a2enmod headers rewrite

COPY --chown=www-data . /var/www/html

RUN php phing.phar -verbose

CMD php phing.phar deploy -verbose ; apache2-foreground
